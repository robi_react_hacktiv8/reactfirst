import React, {Component} from "react";

class Home extends Component {
    render(){
        return(
            <div className="content">
                <h2>Home</h2>
                <p>Create React App is an officially supported way to create single-page React applications. It offers a modern build setup with no configuration.</p>
            </div>
        );
    }
}

export default Home;
